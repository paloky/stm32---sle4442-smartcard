#ifndef SLE_H_
#define SLE_H_

#include "main.h"
#include <sys/types.h>
#include <stdbool.h>

#define delay_cnt 6   // For 48MHz Clock
#define delayUS_ASM(us) do {\
	asm volatile (	"MOV R0,%[loops]\n\t"\
			"1: \n\t"\
			"SUB R0, #1\n\t"\
			"CMP R0, #0\n\t"\
			"BNE 1b \n\t" : : [loops] "r" (delay_cnt*us) : "memory"\
		      );\
} while(0)

// DEFINE DELAYS
#define ck_delay() delayUS_ASM(25)
#define ck_delay_front() delayUS_ASM(4)
#define ck_delay_reset() delayUS_ASM(50)



// DEFINE THE HARDWARE PINOUTS
#define get_io HAL_GPIO_ReadPin(T_IO_GPIO_Port, T_IO_Pin)
#define card_detect HAL_GPIO_ReadPin(T_DT_GPIO_Port, T_DT_Pin)
#define set_ck_1 HAL_GPIO_WritePin(T_CLK_GPIO_Port, T_CLK_Pin, GPIO_PIN_SET);
#define set_ck_0 HAL_GPIO_WritePin(T_CLK_GPIO_Port, T_CLK_Pin, GPIO_PIN_RESET);
#define set_rst_1 HAL_GPIO_WritePin(T_RST_GPIO_Port, T_RST_Pin, GPIO_PIN_SET);
#define set_rst_0 HAL_GPIO_WritePin(T_RST_GPIO_Port, T_RST_Pin, GPIO_PIN_RESET);
#define set_io_1 HAL_GPIO_WritePin(T_IO_GPIO_Port, T_IO_Pin, GPIO_PIN_SET);
#define set_io_0 HAL_GPIO_WritePin(T_IO_GPIO_Port, T_IO_Pin, GPIO_PIN_RESET);



#define SLE_CMD_DUMP_MEMORY 0x30
#define SLE_CMD_DUMP_SECMEM 0x31
#define SLE_CMD_DUMP_PRT_MEMORY 0x34
#define SLE_CMD_COMPARE_VERIFICATION_DATA 0x33
#define SLE_CMD_UPDATE_SECMEM 0x39
#define SLE_CMD_UPDATE_MEMORY 0x38


/*!
  \struct sle_t sle.h "sle.h"
  
  \brief The main struct which represent the status of the card.
  Keep in mind AtMega chips have only 1 KB RAM, dumping all the
  card memory into RAM may cause an out of memory.  
  \var uint8_t *sle_t::atr
  
  \brief ptr to a 4 bytes ATR header returned by the ATZ command.
  \var uint8_t *sle_t::main_memory
  
  \brief ptr to a 256 bytes copy of the SLE 4442 memory.
  \var uint8_t *sle_t::protected_memory
  
  \brief ptr to a 32 bytes copy of the SLE 4442 memory.
  \var uint8_t* sle_t::security_memory
  
  \brief 4 bytes
  \var uint8_t* sle_t::ck_proc
  
  \brief processing clock counts
  \var uint8_t sle_t::card_present
  
  \brief card present 0=no; 1=yes
  \var uint8_t sle_t::auth
  
  \brief card auth 1 - ok, 0 - writing forbidden
  auth ok, I can write
 */

struct sle_t {
	uint8_t *atr;
	uint8_t *main_memory;
	uint8_t *protected_memory;
	uint8_t *security_memory;
	uint8_t *ck_proc;
	uint8_t card_present;
	uint8_t auth;
};



struct sle_t* sle_init(void);
uint8_t check_sle_atr(struct sle_t *sle);
void sle_free(struct sle_t *sle);
void sle_reset(uint8_t *atr);
uint8_t sle_present(struct sle_t *sle);
void sle_dump_memory(uint8_t *mm);
void sle_dump_protect_memory(uint8_t *mm);
void sle_dump_secure_mem(uint8_t *mm);
void sle_dump_all_mem(struct sle_t *sle);
void sle_change_pin(struct sle_t *sle, const uint8_t pin1, const uint8_t pin2, const uint8_t pin3);
void sle_auth(struct sle_t *sle, const uint8_t pin1, const uint8_t pin2, const uint8_t pin3);
void sle_write_memory(struct sle_t *sle, const uint8_t addr, const uint8_t len);
void sle_write_secure_mem(struct sle_t *sle);




#endif
