# STM32 - SLE4442 SmartCard

SmartCard SLE4442 C Library for STM32

In the SLE4442.h, is where you config the HAL Hardware of you system.



CHANGE THIS WITH YOUR APROPIATE HARDWARE DESIGN:

#define get_io HAL_GPIO_ReadPin(T_IO_GPIO_Port, T_IO_Pin)

#define card_detect HAL_GPIO_ReadPin(T_DT_GPIO_Port, T_DT_Pin)

#define set_ck_1 HAL_GPIO_WritePin(T_CLK_GPIO_Port, T_CLK_Pin, GPIO_PIN_SET);

#define set_ck_0 HAL_GPIO_WritePin(T_CLK_GPIO_Port, T_CLK_Pin, GPIO_PIN_RESET);

#define set_rst_1 HAL_GPIO_WritePin(T_RST_GPIO_Port, T_RST_Pin, GPIO_PIN_SET);

#define set_rst_0 HAL_GPIO_WritePin(T_RST_GPIO_Port, T_RST_Pin, GPIO_PIN_RESET);

#define set_io_1 HAL_GPIO_WritePin(T_IO_GPIO_Port, T_IO_Pin, GPIO_PIN_SET);

#define set_io_0 HAL_GPIO_WritePin(T_IO_GPIO_Port, T_IO_Pin, GPIO_PIN_RESET);



Also, you need to config the "delay_cnt" define according with you processor speed to obtain an appropiate uS delay

#define delay_cnt 6   // For 48MHz Clock

I put an Example in the main.c file.