
/*!
  Low level bit banging driver to read and write to
  Sle4442 card.
  */

#include <stdint.h>
#include <stdlib.h>
#include "SLE4442.h"


#define OUT 2
#define IN 3



// PRIVATE DECLARATIONS
void ck_pulse(void);
void set_io(const uint8_t io);
void send_start(void);
void send_stop(void);
uint8_t read_byte(void);
void send_byte(uint8_t byte);
void send_rst(uint8_t *atr);
void send_cmd(const uint8_t control, const uint8_t address, const uint8_t data);
uint8_t processing(void);



/*!
 * Inicialitzar la estructura i la memoria de la tarjeta
 *
 * \return sle Punter amb la estructura.
 */
struct sle_t* sle_init(void)
{
	struct sle_t *sle;

	sle = malloc(sizeof(struct sle_t));
	sle->atr = malloc(4);
	sle->main_memory = malloc(256);
	sle->protected_memory = malloc(4);
	sle->security_memory = malloc(4);
	sle->ck_proc = malloc(5);
	//sle_enable_port();

	return(sle);
}


/*!
 * Comprava la sequencia de reset retornada per la tarjeta
 *
 * \param sle Estructura de la tarjeta.
 * \return '1' si la sequencia es correcte.
*/

uint8_t check_sle_atr(struct sle_t *sle)
{
	uint8_t atr[4] = {0xA2, 0x13, 0x10, 0x91};

	if (memcmp(sle->atr, &atr, 4))
		return(0);
	else
		return(1);
}


/*!
 * Allibera la memoria guardada de la estructura
 *
 * \param sle La estructura.
 * \note En principi, aquesta funció no s'hauria de cridar mai.
 */
void sle_free(struct sle_t *sle)
{
	//sle_disable_port();
	free(sle->ck_proc);
	free(sle->security_memory);
	free(sle->protected_memory);
	free(sle->main_memory);
	free(sle->atr);
	free(sle);
}


/*!
 * \brief Reset Card
 *
 * \param atr Punter per guardar els 4 bytes retornats en la sequencia de reset.
 */
void sle_reset(uint8_t *atr)
{
	send_rst(atr);
}


/*!
 * Comprova si la tarjeta esta present en el lector
 *
 * \param sle La estructura de la tarejta.
 * \return Card present or not.
 */
uint8_t sle_present(struct sle_t *sle)
{
	if (card_detect)
		sle->card_present=0;
	else
		sle->card_present=1;

	return(sle->card_present);
}


/*!
 * Llegir la memoria de la tarjeta a la estructura mm de la tarjeta
 *
 * \param mm L'area de guardat de la memoria.
 * \warning La estructira on volar la memoria, ja ha d'estar alocatada amb almenys 256 bytes.
 */
void sle_dump_memory(uint8_t *mm)
{
	int i;

	send_cmd(SLE_CMD_DUMP_MEMORY, 0, 0);

	for (i=0; i<256; i++)
	{
		*(mm+i) = read_byte();
	}

	ck_pulse(); 
}




/*!
 * Llegir la memoria protegida.
 *
 * \param mm Punter de memoria on guardar els 4 bytes retornats.
 * \warning Aquest espai de memoria ja te que estar alocatat.
 */
void sle_dump_protect_memory(uint8_t *mm)
{
	uint8_t i;

	send_cmd(SLE_CMD_DUMP_PRT_MEMORY, 0, 0);

	for (i=0; i<4; i++)
	{
		*(mm+i) = read_byte();
	}

	ck_pulse();
}



/*!
 * Llegir la memoria Segura.
 *
 * \param mm Punter de memoria on guardar els 4 bytes retornats.
 * \warning  Aquest espai de memoria ja te que estar alocatat.
 */
void sle_dump_secure_mem(uint8_t *mm)
{
	uint8_t i;

	send_cmd(SLE_CMD_DUMP_SECMEM, 0, 0);

	for (i=0; i<4; i++)
	{
		*(mm+i) = read_byte();
	}

	ck_pulse();
}



/*!
 * Volcat de tota la memoria (normal, segura i protegida)
 *
 * \param sle Estructura de la tarejta.
 * \warning Aquest espai de memoria ja te que estar alocatat.
 */
void sle_dump_all_mem(struct sle_t *sle)
{
	sle_dump_memory(sle->main_memory);
	sle_dump_protect_memory(sle->protected_memory);
	sle_dump_secure_mem(sle->security_memory);
}



/**
 * Change the SLE PIN access
 */
void sle_change_pin(struct sle_t *sle, const uint8_t pin1, const uint8_t pin2, const uint8_t pin3)
{
	send_cmd(SLE_CMD_UPDATE_SECMEM, 1, pin1);
	*(sle->ck_proc) = processing();

	send_cmd(SLE_CMD_UPDATE_SECMEM, 2, pin2);
	*(sle->ck_proc) = processing();

	send_cmd(SLE_CMD_UPDATE_SECMEM, 3, pin3);
	*(sle->ck_proc) = processing();

}



/*!
 * Enviem Autentificacio a la Tarjeta
 *
 * \param sle Estructura de la tarjeta.
 * \param pin1 byte 1 of the pin number.
 * \param pin2 byte 2 of the pin number.
 * \param pin3 byte 3 of the pin number.
 */ 
void sle_auth(struct sle_t *sle, const uint8_t pin1, const uint8_t pin2, const uint8_t pin3)
{	
	sle_dump_secure_mem(sle->security_memory);

	// Estem a l'ultim intent de introduir el PIN??
	if (*(sle->security_memory) == 1 || *(sle->security_memory)== 2 || *(sle->security_memory) == 4)
	{
		sle->auth = 0;
		return;
	}
	else
	{
		// Decrementa el valor
		if (*(sle->security_memory) == 7) *(sle->security_memory)=6;
		else if (*(sle->security_memory) == 6) *(sle->security_memory)=4;
		else if (*(sle->security_memory) == 5) *(sle->security_memory)=4;
		else if (*(sle->security_memory) == 3) *(sle->security_memory)=1;

		send_cmd(SLE_CMD_UPDATE_SECMEM, 0, *(sle->security_memory));
		*(sle->ck_proc) = processing();

		/* write 0 to bit 3 */
		//send_cmd(SLE_CMD_UPDATE_SECMEM, 0, 3);
		//*(sle->ck_proc) = processing();

		/* Compare 3 byte PIN */
		send_cmd(SLE_CMD_COMPARE_VERIFICATION_DATA, 1, pin1);
		*(sle->ck_proc + 1) = processing();
		send_cmd(SLE_CMD_COMPARE_VERIFICATION_DATA, 2, pin2);
		*(sle->ck_proc + 2) = processing();
		send_cmd(SLE_CMD_COMPARE_VERIFICATION_DATA, 3, pin3);
		*(sle->ck_proc + 3) = processing();

		// Reseteja el Numero de Intents
		send_cmd(SLE_CMD_UPDATE_SECMEM, 0, 0xff);
		*(sle->ck_proc + 4) = processing();

		// Tornem a llegir la memoria Segura
		sle_dump_secure_mem(sle->security_memory);
	}

	// if error = 7 Vol dir que ens hem authentica correctament
	if (*(sle->security_memory) == 7)
	{
		sle->auth = 1;
	}
	else
	{
		sle->auth = 0;
	}
}




/*!
 * Escriure un grup de bytes a la memoria de la tarjeta.
 *
 * 
 * \param sle Estructura de la tarjeta.
 * \param base Posicio d'inici de la memoria a escriure.
 * \param len Numero de bytes a escriure a la tarjeta.
 */
void sle_write_memory(struct sle_t *sle, const uint8_t base, const uint8_t len)
{
	uint8_t i, addr;

	if (sle->auth)
	{
		for (i=0; i<len; i++) 
		{
			addr = base + i;
			send_cmd(SLE_CMD_UPDATE_MEMORY, addr, *(sle->main_memory + addr));
			*(sle->ck_proc) = processing();
		}
	}
}



/*!
 * Escriu la memoria Segura
 *
 * Copia els 4 bytes de la memoria segura a la tarjeta
 *
 * \param sle Estructura de la tarjeta.
 *
 */
void sle_write_secure_mem(struct sle_t *sle)
{
	uint8_t i;

	if (sle->auth)
	{
		for (i=0; i<4; i++)
		{
			send_cmd(SLE_CMD_UPDATE_SECMEM, i, *(sle->security_memory + i));
			*(sle->ck_proc + i) = processing();
		}
	}
}






//**************************************
//**************************************
// PRIVATE FUNCTIONS
//**************************************
//**************************************

/*!
 * Single clock pulse.
 *
 * Aquesta funció genera un clock a la linia T_CLK
 */
void ck_pulse(void)
{
	set_ck_1;
	ck_delay();
	set_ck_0;
	ck_delay();
}


/*!
 * Set's I/O line to one of: IN, OUT, 0 or 1.
 *
 * IN: The port is in input, the subsequent call with:
 *  0 - Disable internal pull-up resistor.
 *  1 - Enable internal pull-up resistor.
 *
 * OUT: The port in in output mode, the subsequent call with:
 *  0 - Line is logic 0 (GND).
 *  1 - Line is logic 1 (+5V).
 *
 * \param io  can be 0, 1, IN, OUT
 */
void set_io(const uint8_t io)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	switch (io)
	{
	case 0:
		{
			set_io_0;
			break;
		}
	case 1:
		{
			set_io_1;
			break;
		}
	case OUT:
		{
			//Configure GPIO pins : PBPin as OUTPUT
			GPIO_InitStruct.Pin = T_IO_Pin;
			GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
			HAL_GPIO_Init(T_IO_GPIO_Port, &GPIO_InitStruct);
			break;
		}
	//case IN:
	default:
		{
			//Configure GPIO pin : PtPin as INPUT
			GPIO_InitStruct.Pin = T_IO_Pin;
			GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
			GPIO_InitStruct.Pull = GPIO_NOPULL;
			GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
			HAL_GPIO_Init(T_IO_GPIO_Port, &GPIO_InitStruct);
			break;
		}
	}

}


/*!
 * Envia Sequencia de START
 *
 * \La Linia IO Ha de estar com a INPUT.
 */
void send_start(void)
{
	set_ck_0; 
	set_io(OUT);
	set_io(1);
	set_ck_1;
	ck_delay();
	set_io(0);
	ck_delay_front();
	set_ck_0;
	ck_delay();
}



/*!
 * Envia Sequencia de STOP
 *
 * \La Linia IO ha de estar com INPUT
 */
void send_stop(void)
{
	set_io(0);
	ck_delay_front();
	set_ck_1;
	ck_delay_front();
	set_io(IN); 
	ck_delay();
	set_ck_0;
	ck_delay();
}


/*!
 * Llegir un byte
 *
 * Aquesta funcio, genera 8 clocks on a cada un dells, llegim el bit retornat
 *
 * \return El Byte llegit
 * \note La linia T_IO ja ha d'estar configurada com a INPUT.
 */
uint8_t read_byte(void)
{
	uint8_t i;
	uint8_t byte = 0;

	// lectura 8 bits
	for (int x=0; x<8; x++)
	{
		set_ck_1;

		if(get_io) {byte|=0x80;}
		ck_delay();

		if (x < 7){	byte = byte>>1;}

		set_ck_0;

		ck_delay();
	}

	return(byte);
}




/*!
 * Escriu un byte
 *
 * Aquesta funció Escriu un byte - bit a bit.
 * El bit es escrit a la fase 0 del clock.
 *
 * \param byte El Byte a enviar
 * \note La linia T_IO, ja ha d'estar configurada com a OUTPUT.
 */
void send_byte(uint8_t byte)
{
	uint8_t i;

	for (i=0; i<8; i++) 
	{
		//if (byte & _BV(i))
		if (byte&0x01)
			set_io(1);
		else
			set_io(0);

		byte = byte>>1;

		ck_delay_front();
		set_ck_1;
		ck_delay();
		set_ck_0;
		ck_delay();
	}
}



/*!
 * Enviem la sequencia de RESET a la tarjeta
 *
 * \param *atr Punter a la memoria on retornar els 4 byte ATR retornats despres del Reset.
 *
 */
void send_rst(uint8_t *atr)
{
	uint8_t i;

	// Configurem el T_IO com a entrada
	set_io(IN);
	set_rst_1;
	ck_delay_front();
	set_ck_1;
	ck_delay_reset();
	set_ck_0;
	ck_delay_front();
	set_rst_0;
	ck_delay();

	// llegim els bytes retornats
	for (i=0; i<4; i++)
	{		
		*(atr+i) = read_byte();
	}
}



/*!
 * Envia una comanda a la tarjeta
 * 
 * \param control Byte de control.
 * \param address Byte de adreça.
 * \param data Byte de dada.
 * \warning Despres de la comanda, la linia IO es manté en mode INPUT.
 */
void send_cmd(const uint8_t control, const uint8_t address, const uint8_t data)
{
	send_start();
	send_byte(control);
	send_byte(address);
	send_byte(data);
	send_stop();
}


/*!
 * Esperem a que la tarjeta processi la comanda.
 *
 * \return El numero de clock esperat.
 * \warning Si la tarjeta te algun problema i no rebem cap bit per T_IO, el sistema es penja aquí.
 */
uint8_t processing(void)
{
	uint8_t i = 0;

	//while (!(SLE_PIN & _BV(SLE_IO)))
	while (!get_io)
	{
		ck_pulse();
		i++;
	}

	ck_pulse();
	return (i);
}


