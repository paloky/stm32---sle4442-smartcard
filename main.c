


void read_write_smartcard_4442()
{
	// Init struct pointers and memory alocation
	struct sle_t *sle;
	sle = sle_init();

	// Send Reset Sequence
	sle_reset(sle->atr);

	// Check if Reset response it's OK
	if (check_sle_atr(sle))
	{
		// Read all memory map
		sle_dump_all_mem(sle);

		// Send the PIN to can write.
		sle_auth(sle, PIN_CODE_1, PIN_CODE_2, PIN_CODE_3);

		// If sended PIN was OK, now, we can write new values
		if (sle->auth)
		{

			// TODO: Modify the memory map of sle struct.
			// Example:
			// *(sle->main_memory+10) = 0x22;
			// *(sle->main_memory+11) = 0x23;
			//
			// WARNING !!! 
			// Caution of don't modify the 4 first bytes of the Memory where allocate the RESET answers.
			// If you change this values for error, you need change the check_sle_atr() Function.
			
			// Write the new values to SmartCard.
			sle_write_memory(sle, 0x00, 0xFF);
		}

	}

	// Free alocated memory
	sle_free(sle);
}

